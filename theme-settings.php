<?php

/**
* @file
* Contains theme configiration form elements.
* IMPORTANT WARNING: DO NOT MODIFY THIS FILE, UNLESS YOU KNOW WHAT YOU ARE DOING.
*/

// Include the definition of synfox_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'synfox') . '/inc/template.theme-defaults.inc';

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   An array of saved settings for this theme.
* @param $subtheme_defaults
*   Allow a subtheme to override the default values.
* @return
*   A form array.
*/
function synfox_settings($saved_settings, $subtheme_defaults = array()) {
	// Add the form's CSS
	drupal_add_css(drupal_get_path('theme', 'synfox') . '/css/theme-settings.css', 'theme');
	
	// Add javascript to show/hide optional settings
	drupal_add_js(drupal_get_path('theme', 'synfox') . '/js/theme-settings.js', 'theme');
	
	// Get the default values from the .info file.
	$defaults = synfox_theme_get_default_settings('synfox');
	
	// Merge the saved variables and their default values.
	$settings = array_merge($defaults, $saved_settings);
	
	/*
	* Create the form using Forms API
	*/
	$form['synfox-div-opening'] = array(
		'#value'         => '<div id="synfox-settings">',
	);
	
	$form['synfox_block_editing'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Show block editing on hover'),
		'#description'   => t('When hovering over a block, privileged users will see block editing links.'),
		'#default_value' => $settings['synfox_block_editing'],
	);
	
	$form['breadcrumb'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Breadcrumb settings'),
		'#attributes'    => array('id' => 'synfox-breadcrumb'),
	);
	$form['breadcrumb']['synfox_breadcrumb'] = array(
		'#type'          => 'select',
		'#title'         => t('Display breadcrumb'),
		'#default_value' => $settings['synfox_breadcrumb'],
		'#options'       => array(
											'yes'   => t('Yes'),
											'admin' => t('Only in admin section'),
											'no'    => t('No'),
											),
	);
	$form['breadcrumb']['synfox_breadcrumb_separator'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Breadcrumb separator'),
		'#description'   => t('Text only. Don’t forget to include spaces.'),
		'#default_value' => $settings['synfox_breadcrumb_separator'],
		'#size'          => 5,
		'#maxlength'     => 10,
		'#prefix'        => '<div id="div-synfox-breadcrumb-collapse">', // jquery hook to show/hide optional widgets
	);
	$form['breadcrumb']['synfox_breadcrumb_home'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Show home page link in breadcrumb'),
		'#default_value' => $settings['synfox_breadcrumb_home'],
	);
	$form['breadcrumb']['synfox_breadcrumb_trailing'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Append a separator to the end of the breadcrumb'),
		'#default_value' => $settings['synfox_breadcrumb_trailing'],
		'#description'   => t('Useful when the breadcrumb is placed just before the title.'),
	);
	$form['breadcrumb']['synfox_breadcrumb_title'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Append the content title to the end of the breadcrumb'),
		'#default_value' => $settings['synfox_breadcrumb_title'],
		'#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
		'#suffix'        => '</div>', // #div-synfox-breadcrumb
	);
	
	$form['themedev'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Theme development settings'),
		'#attributes'    => array('id' => 'synfox-themedev'),
	);
	$form['themedev']['synfox_rebuild_registry'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Rebuild theme registry on every page.'),
		'#default_value' => $settings['synfox_rebuild_registry'],
		'#description'   => t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
		'#prefix'        => '<div id="div-synfox-registry"><strong>' . t('Theme registry:') . '</strong>',
		'#suffix'        => '</div>',
	);
	$form['themedev']['synfox_blueprint_grid'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Display blueprint grid below the content.'),
		'#default_value' => $settings['synfox_blueprint_grid'],
		'#description'   => t('Only select this if you selected a blueprint layout above.'),
		'#prefix'        => '<div id="div-synfox-blueprint"><strong>' . t('Blueprint Grid:') . '</strong>',
		'#suffix'        => '</div>',
	);
	
	$form['designinfo'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Website designer information'),
		'#attributes'    => array('id' => 'div-synfox-designinfo'),
	);
	$form['designinfo']['synfox_hosting'] = array(
		'#type'          => 'select',
		'#title'         => t('Hosting the website'),
		'#default_value' => $settings['synfox_hosting'],
		'#options'       => array(
											'synfox-hosting-yes'   => t('Yes'),
											'synfox-hosting-no'    => t('No'),
											),
	);
	$form['designinfo']['synfox_site_country'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Country'),
		'#description'   => t('Text only. This will be used on the bottom of the site.'),
		'#default_value' => $settings['synfox_site_country'],
		'#size'          => 50,
		'#maxlength'     => 50,
		'#prefix'        => '<div id="div-synfox-country-collapse">', // jquery hook to show/hide optional widgets
	); 
	$form['designinfo']['synfox_site_designer'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Designed by'),
		'#description'   => t('Text only. This will be used on the bottom of the site.'),
		'#default_value' => $settings['synfox_site_designer'],
		'#size'          => 50,
		'#maxlength'     => 50,
		'#prefix'        => '<div id="div-synfox-designed-collapse">', // jquery hook to show/hide optional widgets
	);
	$form['designinfo']['synfox_site_designer_url'] = array(
		'#type'          => 'textfield',
		'#title'         => t('URL'),
		'#description'   => t('Text only. Will be used as the link for "Designed by".'),
		'#default_value' => $settings['synfox_site_designer_url'],
		'#size'          => 50,
		'#maxlength'     => 50,
		'#prefix'        => '<div id="div-synfox-designed-collapse">', // jquery hook to show/hide optional widgets
	);
	
	$form['addons'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Add-on settings'),
		'#attributes'    => array('id' => 'div-synfox-addon'),
	);
	$form['addons']['synfox_admin_icon'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Admin icons <small>(icons-admin.css)</small>'),
		'#default_value' => $settings['synfox_admin_icon'],
		'#description'   => t('Display icon links based on admin area.'),
		'#prefix'        => '<div id="div-synfox-icons"><strong>' . t('Display admin icon:') . '</strong>',
		'#suffix'        => '</div>',
	);
	$form['addons']['synfox_files_icons'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('File icons <small>(icons-files.css)</small>'),
		'#default_value' => $settings['synfox_files_icons'],
		'#description'   => t('Display icon links based on file type.'),
		'#prefix'        => '<div id="div-synfox-icons"><strong>' . t('Display files icon:') . '</strong>',
		'#suffix'        => '</div>',
	);
	$form['addons']['synfox_proto_icons'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Protocol icons <small>(icons-proto.css)</small>'),
		'#default_value' => $settings['synfox_proto_icons'],
		'#description'   => t('Display icon links based on protocol.'),
		'#prefix'        => '<div id="div-synfox-icons"><strong>' . t('Display protocol icons:') . '</strong>',
		'#suffix'        => '</div>',
	);
	$form['addons']['synfox_addons_fancy_type'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Fancy Type <small>(fancy-type.css)</small>'),
		'#default_value' => $settings['synfox_addons_fancy_type'],
		'#description'   => t('Lots of pretty advanced classes for manipulating text.'),
		'#prefix'        => '<div id="div-synfox-fancy-type"><strong>' . t('Fancy Type:') . '</strong>',
		'#suffix'        => '</div>',
	);
	
	$form['synfox-div-closing'] = array(
		'#value'         => '</div>',
	);
	
	// Return the form
	return $form;
}
