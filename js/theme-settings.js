$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-synfox-breadcrumb').change(
    function() {
      div = $('#div-synfox-breadcrumb-collapse');
      if ($('#edit-synfox-breadcrumb').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-synfox-breadcrumb').val() == 'no') {
    $('#div-synfox-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-synfox-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-synfox-breadcrumb-trailing');
      if ($('#edit-synfox-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-synfox-breadcrumb-title').change();
} );
