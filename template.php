<?php

/**
* @file
* Contains theme override functions and preprocess functions for the synfox theme.
* IMPORTANT WARNING: DO NOT MODIFY THIS FILE, UNLESS YOU KNOW WHAT YOU ARE DOING.
*/

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('synfox_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}


// Add stylesheets only needed when synfox is the active theme.
if ($GLOBALS['theme'] == 'synfox') { // If we're in the main theme
	if (theme_get_setting('synfox_admin_icon')) {
		drupal_add_css(drupal_get_path('theme', 'synfox') . '/css/icons-admin.css', 'theme', 'screen');
	}
	
	if (theme_get_setting('synfox_files_icons')) {
		drupal_add_css(drupal_get_path('theme', 'synfox') . '/css/icons-files.css', 'theme', 'screen');
	}
	
	if (theme_get_setting('synfox_proto_icons')) {
		drupal_add_css(drupal_get_path('theme', 'synfox') . '/css/icons-proto.css', 'theme', 'screen');
	}
	
	if (theme_get_setting('synfox_addons_fancy_type')) {
		drupal_add_css(drupal_get_path('theme', 'synfox') . '/css/fancy-type.css', 'theme', 'all');
	}
}

/**
 * Implements HOOK_theme().
 */
function synfox_theme(&$existing, $type, $theme, $path) {
  include_once './' . drupal_get_path('theme', 'synfox') . '/inc/template.theme-defaults.inc';
  return _synfox_theme($existing, $type, $theme, $path);
}
	
/**
* Return a themed breadcrumb trail.
*
* @param $breadcrumb
*   An array containing the breadcrumb links.
* @return
*   A string containing the breadcrumb output.
*/
function synfox_breadcrumb($breadcrumb) {
	// Determine if we are to display the breadcrumb.
	$show_breadcrumb = theme_get_setting('synfox_breadcrumb');
	if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {
		// Optionally get rid of the homepage link.
		$show_breadcrumb_home = theme_get_setting('synfox_breadcrumb_home');
		if (!$show_breadcrumb_home) {
			array_shift($breadcrumb);
		}
		
		// Return the breadcrumb with separators.
		if (!empty($breadcrumb)) {
			$breadcrumb_separator = theme_get_setting('synfox_breadcrumb_separator');
			$trailing_separator = $title = '';
			if (theme_get_setting('synfox_breadcrumb_title')) {
				$trailing_separator = $breadcrumb_separator;
				$title = menu_get_active_title();
			}
			elseif (theme_get_setting('synfox_breadcrumb_trailing')) {
				$trailing_separator = $breadcrumb_separator;
			}
			return '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . "$trailing_separator$title</div>";
		}
	}
	// Otherwise, return an empty string.
	return '';
}
	
/**
* Implements theme_menu_item_link()
*/
function synfox_menu_item_link($link) {
	if (empty($link['localized_options'])) {
		$link['localized_options'] = array();
	}
	// If an item is a LOCAL TASK, render it as a tab
	if ($link['type'] & MENU_IS_LOCAL_TASK) {
		$link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
		$link['localized_options']['html'] = TRUE;
	} 
	return l($link['title'], $link['href'], $link['localized_options']);
}
	
/**
* Return a themed set of links.
*
* @param $links
*   A keyed array of links to be themed.
* @param $attributes
*   A keyed array of attributes
* @return
*   A string containing an unordered list of links.
*/
function synfox_links($links, $attributes = array('class' => 'links')) {
	$output = '';
	if (count($links) > 0) {
		$output = '<ul'. drupal_attributes($attributes) .'>';
		$num_links = count($links);
		$i = 1;
		foreach ($links as $key => $link) {
		$class = $key;
		// Add first, last and active classes to the list of links to help out themers.
		if ($i == 1) {
			$class .= ' first';
		}
		if ($i == $num_links) {
			$class .= ' last';
		}
		if (isset($link['href']) && $link['href'] == $_GET['q']) {
			$class .= ' active';
		}
		$output .= '<li class="'. $class .'">';
		
		if (isset($link['href'])) {
			// Pass in $link as $options, they share the same keys.
			$link['html'] = TRUE;
			$output .= l('<span>'. $link['title'] .'</span>', $link['href'], $link);
		}
		elseif (!empty($link['title'])) {
			// Some links are actually not links, but we wrap these in <span> for adding title and class attributes
			if (empty($link['html'])) {
				$link['title'] = check_plain($link['title']);
			}
			$span_attributes = '';
			if (isset($link['attributes'])) {
				$span_attributes = drupal_attributes($link['attributes']);
			}
			$output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
			}
			$i++;
			$output .= "</li>\n";
		}
		$output .= '</ul>';
	}
	return $output;
}
	
	
/**
* Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
*/
function synfox_menu_local_tasks() {
	$output = '';
	if ($primary = menu_primary_local_tasks()) {
		$output .= '<ul class="tabs primary clear-block">' . $primary . '</ul>';
	}
	if ($secondary = menu_secondary_local_tasks()) {
		$output .= '<ul class="tabs secondary clear-block">' . $secondary . '</ul>';
	}
	return $output;
}


/**
* Override or insert variables into the page templates.
* @param $vars
*   An array of variables to pass to the theme template.
* @param $hook
*   The name of the template being rendered ("page" in this case.)
*/
function synfox_preprocess_page(&$vars, $hook) {
	// Add copywrite information.
	if (theme_get_setting('synfox_hosting') == 'synfox-hosting-yes') {
		$output = '<p>'. theme_get_setting('synfox_site_country') .' Web Design and Hosting by <a href="'. theme_get_setting('synfox_site_designer_url') .'">' . theme_get_setting('synfox_site_designer') . '</a>.</p>';
	}
	else {
		$output = '<p>'. theme_get_setting('synfox_site_country') .' Web Design by <a href="'. theme_get_setting('synfox_site_designer_url') .'">' . theme_get_setting('synfox_site_designer') . '</a>.</p>';
	}
	$output .= '<p>Copyright &copy; ' . date("Y") . ' ' . $vars['site_name'] . '. All Rights Reserved.</p>';
	$vars['designed_by'] = $output;																															
	
	//Setup classess for primary links and search box
	if (($vars['primary_links']) && ($vars['search_box'])) {
		$vars['primary_menu_classes'] = ' span-20';
		$vars['search_box_classes'] = 'span-4 last';
	}
	else {
		if ($vars['primary_links']) {
			$vars['primary_menu_classes'] = ' span-24 last';
		}
		elseif ($vars['search_box']) {
			$vars['search_box_classes'] = 'span-24 last';
		}
	}
	
	// determine Blueprint grid 24 layout
	if ($vars['layout'] == 'both') {				// 3 columns
		if ($vars['is_front']) {					//Add round corners to sidebars for the front page.
			$vars['center_classes'] = 'col-center span-14 last';
			$vars['left_classes'] .= 'col-left span-5';
			$vars['right_classes'] .= 'col-right span-5';
		}
		else {
			$vars['center_classes'] = 'col-center span-14';
			$vars['left_classes'] = 'col-left span-5';
			$vars['right_classes'] = 'col-right span-5 last';
		}
			$vars['body_classes'] .= ' col-3 ';
	}
	elseif ($vars['layout'] != 'none') {		// 2 columns	
		if ($vars['layout'] == 'left') {			// left column & center
			if ($vars['is_front']) {
				$vars['left_classes'] = 'col-left span-6 last';
				$vars['center_classes'] = 'col-center span-18';
			}
			else {
				$vars['left_classes'] = 'col-left span-5';
				$vars['center_classes'] = 'col-center span-19 last';
			}
		}
		elseif ($vars['layout'] == 'right') {	// center & right column
			if ($vars['is_front']) {
				$vars['center_classes'] = 'col-center span-18';
				$vars['right_classes'] = 'col-right span-6 last';
			}
			else {
				$vars['center_classes'] = 'col-center span-19';
				$vars['right_classes'] = 'col-right span-5 last';
			}
		}
		$vars['body_classes'] .= ' col-2 ';
	}
	else {												// 1 column
		$vars['center_classes'] = 'col-center span-24 last';
		$vars['body_classes'] .= ' col-1 ';
	}
	
	// Optionally add the blueprint grid.
	if (theme_get_setting('synfox_blueprint_grid')) {
		$vars['page_classes'] = ' showgrid'; 
	}
	
	// Classes for body element. Allows advanced theming based on context
	// (home page, node of certain type, etc.)
	$classes = split(' ', $vars['body_classes']);
	// Remove the mostly useless page-ARG0 class.
	if ($index = array_search(preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. drupal_strtolower(arg(0))), $classes)) {
		unset($classes[$index]);
	}
	if (!$vars['is_front']) {
		// Add unique class for each page.
		$path = drupal_get_path_alias($_GET['q']);
		$classes[] = synfox_id_safe('page-' . $path);
		// Add unique class for each website section.
		list($section, ) = explode('/', $path, 2);
		if (arg(0) == 'node') {
			if (arg(1) == 'add') {
				$section = 'node-add';
			}
			elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
				$section = 'node-' . arg(2);
			}
		}
		$classes[] = synfox_id_safe('section-' . $section);
	}
	
	$vars['body_classes_array'] = $classes;
	$vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.
	
	/**
	* Solve the Internet Explorer limitation of loading not more then 30 CSS files per page.
	* This solves the Internet Explorer limitation of loading 
	* not more then 30 CSS files per HTML page  (http://support.microsoft.com/kb/262161).
	*/
	$preprocess_css = variable_get('preprocess_css', 0);
	if (!$preprocess_css) {
		$styles = '';
		foreach ($vars['css'] as $media => $types) {
			$import = '';
			$counter = 0;
			foreach ($types as $files) {
				foreach ($files as $css => $preprocess) {
				if (file_exists($css)) {
					$import .= '@import "'. base_path() . $css .'";'."\n";
					$counter++;
					if ($counter == 25) {
						$styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
						$import = '';
						$counter = 0;
					}
				}
			}
		}
		if ($import) {
			$styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
		}
		}
		if (!empty($vars['conditional_styles'])) {
			$styles .= "\n". $vars['conditional_styles'];
		}    
		if ($styles) {
			$vars['styles'] = $styles;
		}
	}		
}

/**
* Override or insert variables into the node templates.
* @param $vars
*   An array of variables to pass to the theme template.
* @param $hook
*   The name of the template being rendered ("node" in this case.)
*/
function synfox_preprocess_node(&$vars, $hook) {
	// Special classes for nodes
	$classes = array('node');
	if ($vars['sticky']) {
		$classes[] = 'sticky';
	}
	if (!$vars['status']) {
		$classes[] = 'node-unpublished';
		$vars['unpublished'] = TRUE;
	}
	else {
		$vars['unpublished'] = FALSE;
	}
	if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
		$classes[] = 'node-mine'; // Node is authored by current user.
	}
	if ($vars['teaser']) {
		$classes[] = 'node-teaser'; // Node is displayed as teaser.
	}
	// Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
	$classes[] = synfox_id_safe('node-type-' . $vars['type']);
	$vars['classes'] = implode(' ', $classes); // Concatenate with spaces
}

/**
* Override or insert variables into the comment templates.
* @param $vars
*   An array of variables to pass to the theme template.
* @param $hook
*   The name of the template being rendered ("comment" in this case.)
*/
function synfox_preprocess_comment(&$vars, $hook) {
	include_once './' . drupal_get_path('theme', 'synfox') . '/inc/template.comment.inc';
	_synfox_preprocess_comment($vars, $hook);
}

/**
* Override or insert variables into the block templates.
* @param $vars
*   An array of variables to pass to the theme template.
* @param $hook
*   The name of the template being rendered ("block" in this case.)
*/
function synfox_preprocess_block(&$vars, $hook) {
	$block = $vars['block'];
	
	// Special classes for blocks.
	$classes = array('block');
	$classes[] = 'block-' . $block->module;
	$classes[] = 'region-' . $vars['block_zebra'];
	$classes[] = $vars['zebra'];
	$classes[] = 'region-count-' . $vars['block_id'];
	$classes[] = 'count-' . $vars['id'];
	
	$vars['edit_links_array'] = array();
	$vars['edit_links'] = '';
	if (theme_get_setting('synfox_block_editing') && user_access('administer blocks')) {
		include_once './' . drupal_get_path('theme', 'synfox') . '/inc/template.block-editing.inc';
		synfox_preprocess_block_editing($vars, $hook);
		$classes[] = 'with-block-editing';
	}
	// Render block classes.
	$vars['classes'] = implode(' ', $classes);
	
	// Allow the changes the block title when block title is set to 'welcome-username'
	if ($block->subject){
		global $user;
		if($block->subject == 'welcome-username') {
			$block->subject = '<h2 class="title">User Menu</h2><p style="margin:0.2em 0; text-align: center;"><span class="first-word">Welcome</span> ' . check_plain($user->name) . '</p>';
		}
		else {
			$block->subject = '<h2 class="title">'.$block->subject.'</h2>';
		}
	}
}

/**
* Converts a string to a suitable html ID attribute.
* http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
* valid ID attribute in HTML. This function:
* - Ensure an ID starts with an alpha character by optionally adding an 'id'.
* - Replaces any character except alphanumeric characters with dashes.
* - Converts entire string to lowercase.
* @param $string
*   The string
* @return
*   The converted string
*/
function synfox_id_safe($string) {
	// Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
	$string = strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
	// If the first character is not a-z, add 'id' in front.
	if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
		$string = 'id' . $string;
	}
	return $string;
}

/**
* Override comment wrapper to show you must login to comment.
*/
function synfox_comment_wrapper($content, $node) {
	global $user;
	$output = '';
	
	if ($node = menu_get_object()) {
		if ($node->type != 'forum') {
			$count = $node->comment_count .' '. format_plural($node->comment_count, 'comment', 'comments');
			$count = ($count > 0) ? $count : 'No comments';
			$output .= '<h3 id="comment-number">'. $count .'</h3>';
		}
	}
	
	$output .= '<div id="comments">';
	$msg = '';
	if (!user_access('post comments')) {
		$dest = 'destination='. $_GET['q'] .'#comment-form';
		$msg = '<div id="messages"><div class="error-wrapper"><div class="messages error">'. t('Please <a href="!register">register</a> or <a href="!login">login</a> to post a comment.', array('!register' => url("user/register", array('query' => $dest)), '!login' => url('user', array('query' => $dest)))) .'</div></div></div>';
	}
	$output .= $content;
	$output .= $msg;
	
	return $output .'</div>';
}

/**
* Override, use better icons, source: http://drupal.org/node/102743#comment-664157
* Format the icon for each individual topic.
* @ingroup themeable
*/
function synfox_forum_icon($new_posts, $num_posts = 0, $comment_mode = 0, $sticky = 0) {
	// because we are using a theme() instead of copying the forum-icon.tpl.php into the theme
	// we need to add in the logic that is in preprocess_forum_icon() since this isn't available
	if ($num_posts > variable_get('forum_hot_topic', 15)) {
		$icon = $new_posts ? 'hot-new' : 'hot';
	}
	else {
		$icon = $new_posts ? 'new' : 'default';
	}
	
	if ($comment_mode == COMMENT_NODE_READ_ONLY || $comment_mode == COMMENT_NODE_DISABLED) {
		$icon = 'closed';
	}
	
	if ($sticky == 1) {
		$icon = 'sticky';
	}
	
	$output = theme('image', path_to_theme() . "/images/icons/forum_icons/forum-$icon.png");
	
	if ($new_posts) {
		$output = "<a name=\"new\">$output</a>";
	}
	
	return $output;
}

/**
* Override, remove previous/next links for forum topics
* Makes forums look better and is great for performance
* More: http://www.sysarchitects.com/node/70
*/
function synfox_forum_topic_navigation($node) {
	return '';
}

/**
* Trim a post to a certain number of characters, removing all HTML.
*/
function synfox_trim_text($text, $length = 150) {
	// remove any HTML or line breaks so these don't appear in the text
	$text = trim(str_replace(array("\n", "\r"), ' ', strip_tags($text)));
	$text = trim(substr($text, 0, $length));
	$lastchar = substr($text, -1, 1);
	// check to see if the last character in the title is a non-alphanumeric character, except for ? or !
	// if it is strip it off so you don't get strange looking titles
	if (preg_match('/[^0-9A-Za-z\!\?]/', $lastchar)) {
		$text = substr($text, 0, -1);
	}
	// ? and ! are ok to end a title with since they make sense
	if ($lastchar != '!' && $lastchar != '?') {
		$text .= '...';
	}
	return $text;
}

function synfox_preprocess_search_theme_form(&$vars, $hook) {
  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('Search');
 
  // Set a default value for the search box
  $vars['form']['search_theme_form']['#value'] = t('Search this Site');
 
  // Add a custom class to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => 'NormalTextBox txtSearch',
  'onfocus' => "if (this.value == 'Search this Site') {this.value = '';}",
  'onblur' => "if (this.value == '') {this.value = 'Search this Site';}");
 
  // Change the text on the submit button
  //$vars['form']['submit']['#value'] = t('Go');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

  $vars['form']['submit']['#type'] = 'image_button';
  $vars['form']['submit']['#src'] = path_to_theme() . '/images/search/search-button.png';

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}

/*!
 * Dynamic display block preprocess functions
 * Copyright (c) 2008 - 2009 P. Blaauw All rights reserved.
 * Version 1.6 (01-OCT-2009)
 * Licenced under GPL license
 * http://www.gnu.org/licenses/gpl.html
 */

 /**
 * Override or insert variables into the ddblock_cycle_block_content templates.
 *   Used to convert variables from view_fields to slider_items template variables
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * 
 */
function synfox_preprocess_ddblock_cycle_block_content(&$vars) {
  if ($vars['output_type'] == 'view_fields') {
    $content = array();
    // Add slider_items for the template 
    // If you use the devel module uncomment the following line to see the theme variables
    // dsm($vars['settings']['view_name']);  
    // dsm($vars['content'][0]);
    // If you don't use the devel module uncomment the following line to see the theme variables
    // drupal_set_message('<pre>' . var_export($vars['settings']['view_name'], true) . '</pre>');
    // drupal_set_message('<pre>' . var_export($vars['content'][0], true) . '</pre>');
    if ($vars['settings']['view_name'] == 'news_items') {
      if (!empty($vars['content'])) {
        foreach ($vars['content'] as $key1 => $result) {
          // add slide_image variable 
          if (isset($result->node_data_field_pager_item_text_field_image_fid)) {
            // get image id
            $fid = $result->node_data_field_pager_item_text_field_image_fid;
            // get path to image
            $filepath = db_result(db_query("SELECT filepath FROM {files} WHERE fid = %d", $fid));
            //  use imagecache (imagecache, preset_name, file_path, alt, title, array of attributes)
            if (module_exists('imagecache') && is_array(imagecache_presets()) && $vars['imgcache_slide'] <> '<none>'){
              $slider_items[$key1]['slide_image'] = 
              theme('imagecache', 
                    $vars['imgcache_slide'], 
                    $filepath,
                    check_plain($result->node_title));
            }
            else {          
              $slider_items[$key1]['slide_image'] = 
                '<img src="' . base_path() . $filepath . 
                '" alt="' . check_plain($result->node_title) . 
                '"/>';     
            }          
          }
          // add slide_text variable
          if (isset($result->node_data_field_pager_item_text_field_slide_text_value)) {
            $slider_items[$key1]['slide_text'] =  check_markup($result->node_data_field_pager_item_text_field_slide_text_value);
          }
          // add slide_title variable
          if (isset($result->node_title)) {
            $slider_items[$key1]['slide_title'] =  check_plain($result->node_title);
          }
          // add slide_read_more variable and slide_node variable
          if (isset($result->nid)) {
            $slider_items[$key1]['slide_read_more'] =  l('Read more...', 'node/' . $result->nid);
            $slider_items[$key1]['slide_node'] =  base_path() . 'node/' . $result->nid;
          }
        }
      }
    }    
    $vars['slider_items'] = $slider_items;
  }
}  
/**
 * Override or insert variables into the ddblock_cycle_pager_content templates.
 *   Used to convert variables from view_fields  to pager_items template variables
 *  Only used for custom pager items
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 *
 */
function synfox_preprocess_ddblock_cycle_pager_content(&$vars) {
  if (($vars['output_type'] == 'view_fields') && ($vars['pager_settings']['pager'] == 'custom-pager')){
    $content = array();
    // Add pager_items for the template 
    // If you use the devel module uncomment the following lines to see the theme variables
    // dsm($vars['pager_settings']['view_name']);     
    // dsm($vars['content'][0]);     
    // If you don't use the devel module uncomment the following lines to see the theme variables
    // drupal_set_message('<pre>' . var_export($vars['pager_settings'], true) . '</pre>');
    // drupal_set_message('<pre>' . var_export($vars['content'][0], true) . '</pre>');
    if ($vars['pager_settings']['view_name'] == 'news_items') {
      if (!empty($vars['content'])) {
        foreach ($vars['content'] as $key1 => $result) {
          // add pager_item_image variable
          if (isset($result->node_data_field_pager_item_text_field_image_fid)) {
            $fid = $result->node_data_field_pager_item_text_field_image_fid;
            $filepath = db_result(db_query("SELECT filepath FROM {files} WHERE fid = %d", $fid));
            //  use imagecache (imagecache, preset_name, file_path, alt, title, array of attributes)
            if (module_exists('imagecache') && 
                is_array(imagecache_presets()) && 
                $vars['imgcache_pager_item'] <> '<none>'){
              $pager_items[$key1]['image'] = 
                theme('imagecache', 
                      $vars['pager_settings']['imgcache_pager_item'],              
                      $filepath,
                      check_plain($result->node_data_field_pager_item_text_field_pager_item_text_value));
            }
            else {          
              $pager_items[$key1]['image'] = 
                '<img src="' . base_path() . $filepath . 
                '" alt="' . check_plain($result->node_data_field_pager_item_text_field_pager_item_text_value) . 
                '"/>';     
            }          
          }
          // add pager_item _text variable
          if (isset($result->node_data_field_pager_item_text_field_pager_item_text_value)) {
            $pager_items[$key1]['text'] =  check_plain($result->node_data_field_pager_item_text_field_pager_item_text_value);
          }
        }
      }
    }
    $vars['pager_items'] = $pager_items;
  }    
}

function synfox_feed_icon($url) {
  if ($image = theme('image', drupal_get_path('theme', 'synfox') . '/images/rss-icon.png', t('Syndicate content'), t('Syndicate content'))) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image. '</a>';
  }
}