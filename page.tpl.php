<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

	<head>
		<title><?php print $head_title; ?></title>
		<?php print $head; ?>
		<?php print $styles; ?>
		<?php print $scripts; ?>
		<!--[if lt IE 7]>
			<link type="text/css" rel="stylesheet" media="all" href="'<?php print base_path() . path_to_theme(); ?>'/css/ie.css" />
		 <![endif]-->
	</head>
	<body class="<?php print $body_classes; ?>">
		<div id="wrapper">
			<div id="top-section" class="container<?php print $page_classes; ?>">
				<?php if ($navbar_top): ?>
					<div id="navbar-top" class="span-24 last">
						<div id="navbar-top-inner" class="clear-block region region-navbar">
							<?php print $navbar_top; ?>
						</div>
					</div> <!-- /#navbar-top -->
				<?php endif; ?>
				
				<div id="header" class="span-24 last">
					<?php if ($logo): ?>
						<div id="logo" class="span-4">
							<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a>
						</div>
					<?php endif; ?>
					<div id="site-name" class="span-20 last">
						<?php if ($site_name): ?>
							<?php if ($title): ?>
								<div id="site-name">
									<strong><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></strong>
								</div>
							<?php else: ?>
								<h1 id="site-name"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
							<?php endif; ?>
						<?php endif; ?>
				
						<?php if ($site_slogan): ?>
							<div id="site-slogan"><?php print $site_slogan; ?></div>
						<?php endif; ?>
					</div> <!-- /#site-name -->
						
					<?php if ($header): ?>
						<div id="header-blocks" class="region region-header">
							<?php print $header; ?>
						</div> <!-- /#header-blocks -->
					<?php endif; ?>
				</div> <!-- /#header -->
				
				<?php if ($navbar_btm || $primary_links): ?>
					<div id="navbar-btm" class="span-24 last">
						<?php if ($primary_links) : ?>
							<div id="primary-menu">
								<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
							</div>
						<?php endif; ?>
						<?php print $navbar_btm; ?>
					</div> <!-- /#navbar-btm -->
				<?php endif; ?>
			</div> <!-- /#top-section -->
					
			<div id="middle-section" class="container<?php print $page_classes; ?>">
				<div id="middle-section-top" class="span-24 last">
					<div id="middle-section-bottom" class="span-24 last">
						<?php if ($preface_first OR $preface OR $preface_last): ?>
							<div id="preface" class="span-24 last">
								<?php if ($preface_first): ?>
									<div class="first region">
										<?php print $preface_first; ?>
									</div><!-- CLASS first region -->
								<?php endif; ?>
						
								<?php if ($preface): ?>
									<div class="middle region">
										<?php print $preface; ?>
									</div><!-- CLASS middle region -->
								<?php endif; ?>
								
								<?php if ($preface_last): ?>
									<div class="last region">
										<?php print $preface_last; ?>
									</div><!-- CLASS last region -->
								<?php endif; ?>
								
							</div><!-- ID preface -->
						<?php endif; ?>
						
						<?php if ($left): ?>
							<div id="sidebar-left" class="<?php print $left_classes; ?>">
								<div id="sidebar-left-inner" class="region region-left">
									<?php print $left; ?>
								</div> <!-- /#sidebar-left-inner -->
							</div> <!-- /#sidebar-left -->
						<?php endif; ?>
						
						<div id="content" class="<?php print $center_classes; ?>">
							<div id="content-inner" class="region region-content">
								<?php if ($content_top): ?>
									<div id="content-top" class="<?php print $center_classes; ?>">
										<?php print $content_top; ?>
									</div> <!-- /#content-top -->
								<?php endif; ?>
								
								<?php if ($mission): ?>
									<div id="mission"><?php print $mission; ?></div>
								<?php endif; ?>
								
								<?php if ($breadcrumb || $title || $tabs || $help || $messages): ?>
									<div id="content-header">
									
										<?php print $breadcrumb; ?>
										
										<?php if ($title): ?>
											<h1 class="title"><?php print $title; ?></h1>
										<?php endif; ?>
										
										<?php print $messages; ?>
										
										<?php if ($tabs): ?>
											<div class="tabs"><?php print $tabs; ?></div>
										<?php endif; ?>
										
										<?php print $help; ?>
									
									</div> <!-- /#content-header -->
								<?php endif; ?>
								
								<div id="content-area">
									<?php print $content; ?>
								</div>
								
								<?php if ($feed_icons): ?>
									<div class="feed-icons"><?php print $feed_icons; ?></div>
								<?php endif; ?>
								
							</div>  <!--/#content-inner -->
						</div> <!-- /#content -->
						
						<?php if ($right): ?>
							<div id="sidebar-right" class="<?php print $right_classes; ?>">
								<div id="sidebar-right-inner" class="region region-right">
									<?php print $right; ?>
								</div> <!-- /#sidebar-right-inner -->
							</div> <!-- /#sidebar-right -->
						<?php endif; ?>
						
						<?php if ($content_bottom): ?>
							<div id="content-bottom" class="region region-content_bottom span-24 last">
								<div id="content-bottom-inner">
									<?php print $content_bottom; ?>
								</div>
							</div> <!-- /#content-bottom -->
						<?php endif; ?>
						
						<?php if ($postscript_first OR $postscript OR $postscript_last): ?>
							<div id="postscript" class="span-24 last">
							
								<?php if ($postscript_first): ?>
									<div class="first region">
										<?php print $postscript_first; ?>
									</div><!-- CLASS first region -->
								<?php endif; ?>
								
								<?php if ($postscript): ?>
									<div class="middle region">
										<?php print $postscript; ?>
									</div><!-- CLASS middle region -->
								<?php endif; ?>
								
								<?php if ($postscript_last): ?>
									<div class="last region">
										<?php print $postscript_last; ?>
									</div><!-- CLASS last region -->
								<?php endif; ?>
							
							</div><!-- ID postscript -->
						<?php endif; ?>
						
						<div id="footer" class="span-24 last">
							<div id="footer-inner" class="span-24 last">
							
								<?php if ($footer_message): ?>
									<div id="footer-message">
										<?php print $footer_message; ?>
									</div>
								<?php endif; ?>
								
								<?php if ($secondary_links): ?>
									<div id="secondary">
										<?php print theme('links', $secondary_links); ?>
									</div> <!-- /#secondary -->
								<?php endif; ?>
								
								<?php print $footer; ?>
							
							</div> <!-- /#footer-inner -->
						</div> <!-- /#footer -->
						
					</div> <!-- /#middle-section-bottom -->
				</div> <!-- /#middle-section-top -->
			</div> <!-- /#middle-section -->
			
			<div id="bottom-section" class="container<?php print $page_classes; ?>">
				<?php if ($copyright || $search_box): ?>
					<div id="copyright" class="span-24 last">
						<div id="copyright-top" class="span-24 last">
							
							<?php print $copyright; ?>
							
							<?php if ($search_box): ?>
								<div id="search-box">
									<?php print $search_box; ?>
								</div> <!-- /#search-box -->
							<?php endif; ?>
						</div> <!-- /#copyright-top -->
					
						<div id="copyright-bottom" class="span-24 last">
							<?php print $designed_by; ?>
						</div> <!-- /#copyright-bottom -->
					</div> <!-- /#copyright -->
				<?php endif; ?>
			</div>
			
			<?php if ($closure_region): ?>
				<div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
			<?php endif; ?>
			
			<?php print $closure; ?>
		</div>
	</body>
</html>