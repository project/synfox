<?php

/**
 * @file block.tpl.php
 *
 * Theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $block->content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: This is a numeric id connected to each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: A set of CSS classes for the DIV wrapping the block.
     Possible values are: block-MODULE, region-odd, region-even, odd, even,
     region-count-X, and count-X.
 *
 * Helper variables:
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>

<div id="block-<?php print $block->module . '-' . $block->delta; ?>" class="<?php print $classes; ?> <?php if (function_exists('block_class')) print block_class($block); ?>">
	<div class="wrap-corner">
		<div class="t-edge"><div class="l"></div><div class="r"></div></div>
			<div class="l-edge">
				<div class="r-edge clear-block">
					<?php if ($block->subject): ?>
					<?php print $block->subject; ?>
					<?php endif; ?>
					<div class="content">
					<?php print $block->content; ?>
					</div>
					
					<?php print $edit_links; ?>
				</div>
			</div>
		<div class="b-edge"><div class="l"></div><div class="r"></div></div>
	</div>
</div> <!-- /block -->